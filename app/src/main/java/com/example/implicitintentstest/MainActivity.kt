package com.example.implicitintentstest

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add("Web")
        menu?.add("Map")
        menu?.add("Phone Number")

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val uri: Uri
        var intent = Intent()

        when (item.toString()) {
            "Web" -> {
                uri = Uri.parse("https://www.debian.org")
                intent = Intent(Intent.ACTION_VIEW, uri)
            }
            "Map" -> {
                uri = Uri.parse("geo:40.7113399,-74.0263469")
                /*This would have worked as well
                uri = Uri.parse("https://maps.google.com/maps?q = 40.7113399, -74.0263469")*/
                intent = Intent(Intent.ACTION_VIEW, uri)
            }
            "Phone Number" -> {
                uri = Uri.parse("tel:639285083333")
                intent = Intent(Intent.ACTION_DIAL, uri)
            }
        }
        startActivity(intent)
        return true
    }
}
